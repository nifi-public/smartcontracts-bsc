// Fetch the Storage contract data from the Storage.json file

var Endorsable = artifacts.require("./tokens/Endorsable/ERC721Endorsable.sol");
var Seal = artifacts.require("./tokens/Seal/ERC721Seal.sol");
var Ask = artifacts.require("./transactions/Ask.sol");

// JavaScript export
module.exports = function(deployer) {
    // Deployer is the Truffle wrapper for deploying
    // contracts to the network

    // Deploy the contract to the network
    deployer.deploy(Endorsable, "EndorsableTest_1","EE_T1","e_testUri");
    deployer.deploy(Seal, "SealTest_1","SL_T1","s_testUri");
    deployer.deploy(Ask);
}