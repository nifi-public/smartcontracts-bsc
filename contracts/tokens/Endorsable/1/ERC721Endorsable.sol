// SPDX-License-Identifier: BUSL-1.1
pragma solidity ^0.8.9;

import "@openzeppelin/contracts/token/ERC721/extensions/ERC721Burnable.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

import "./IERC721Endorsable.sol";
import "../../Seal/1/IERC721Seal.sol";

contract ERC721Endorsable is ERC721Burnable, IERC721Endorsable, Ownable {

    struct CrossEndorsmentStruct {
        address endorsableAddress;
        uint endorsableNftId;
    }

    struct SealStruct {
        address sealAddress;
        uint sealNftId;
        uint8 sealCorner;
    }

    mapping(uint => SealStruct) private _seals;
    mapping(uint => address) public draftNfts;

    // TokenId => SealAddress => SealTokenId => timestamp
    mapping(uint => mapping(address => mapping(uint => uint))) private _timeLimitedEndorsements;

    // TokenId => crossEndorsementAddress => crossEndorsementId => status
    mapping(uint => mapping(address => mapping(uint => bool))) public _isCrossEndorsed;
    mapping(uint => CrossEndorsmentStruct) private _crossEndorsementBy;

    address private _draftForever;
    address private _forever;

    string private baseURI_;
    uint public lastTokenId;

    //draft
    address[] public draftArray;
    mapping(address => uint) public draftMap;

    function addToDraft(address draftAddress) private {
        if(draftMap[draftAddress] == 0) {
            draftArray.push(draftAddress);
            draftMap[draftAddress] = draftArray.length;
        }
    }

    function removeFromDraft(address draftAddress) private {
        if(draftMap[draftAddress] != 0) {
            address buf = draftArray[draftArray.length-1];
            draftArray[draftMap[draftAddress] - 1] = buf;
            draftArray.pop();
            draftMap[draftAddress] = 0;
        }
    }


    modifier onlyDraftForever {
        require(_draftForever != address(0), "Draft forever is zero address");
        require(_draftForever == _msgSender(), "Only draft forever can call this function");
        _;
    }

    modifier isNotInForever {
        require(_draftForever == address(0), "Token is in draft forever");
        require(_forever == address(0), "Token is in forever");
        _;
    }

    event Endorsed(uint nftId, address sealAddress, uint sealNftId);
    event EndorsedTimeLimited(uint nftId, address sealAddress, uint sealNftId);

    constructor(
        string memory name_,
        string memory symbol_,
        string memory tokenURI_
    ) ERC721(name_, symbol_) { 
        baseURI_ = tokenURI_;
    }

    function mint() public onlyOwner {
        lastTokenId++;
        _mint(msg.sender, lastTokenId);
    }

    function endorse(uint nftId, address sealAddress, uint sealNftId) public override isNotInForever {
        require(_isApprovedOrOwner(_msgSender(), nftId), "isApprovedOrOwner required");
        require(sealAddress != address(0), "Seal is zero address");
        require(_seals[nftId].sealAddress == address(0), "Seal is already set");

        (bool isSuccess, uint8 sealCorner) = IERC721Seal(sealAddress).getEndorsementInfo(sealNftId, address(this), nftId);
        require(isSuccess, "Seal has not endorsed this token");

        _seals[nftId] = SealStruct(sealAddress, sealNftId, sealCorner);

        emit Endorsed(nftId, sealAddress, sealNftId);
    }

    function endorseTimeLimited(uint nftId, address sealAddress, uint sealNftId) public override isNotInForever {
        require(_isApprovedOrOwner(_msgSender(), nftId), "isApprovedOrOwner required");
        require(sealAddress != address(0), "Seal is zero address");

        (bool isSuccess, uint endTime) = IERC721Seal(sealAddress).getTimeLimitedEndorsementInfo(
            sealNftId, address(this), nftId
        );

        require(isSuccess, "Seal has not endorsed this token");

        _timeLimitedEndorsements[nftId][sealAddress][sealNftId] = endTime;

        emit EndorsedTimeLimited(nftId, sealAddress, sealNftId);
    }

    function setInDraft(uint nftId, bool status) public override {
        require(_isApprovedOrOwner(_msgSender(), nftId), "isApprovedOrOwner required");
        if(status) {
            require(_isApprovedOrOwner(_msgSender(), nftId), "isApprovedOrOwner required");
            addToDraft(msg.sender);
            draftNfts[nftId] = msg.sender;
        } else {
            removeFromDraft(msg.sender);
        }
    }

    function crossEndorse(uint nftId, address foreignEndorsableAddress, uint foreignEndorsableNftId) public override isNotInForever {
        require(_isApprovedOrOwner(_msgSender(), nftId), "isApprovedOrOwner required");
        _isCrossEndorsed[nftId][foreignEndorsableAddress][foreignEndorsableNftId] = true;
    }

    function isCrossEndorsedAsMaster(uint nftId, address foreignEndorsableAddress, uint foreignEndorsableNftId) public view returns (bool) {
        return _isCrossEndorsed[nftId][foreignEndorsableAddress][foreignEndorsableNftId];
    }

    function markAsCrossEndorsed(uint nftId, address foreignEndorsableAddress, uint foreignEndorsableNftId) public override isNotInForever {
        require(_isApprovedOrOwner(_msgSender(), nftId), "isApprovedOrOwner required");
        require(foreignEndorsableAddress != address(0), "Endorsable is zero address");
        require(!isCrossEndorsed(nftId), "Endorsable is already endorsed");

        require(ERC721Endorsable(foreignEndorsableAddress).isCrossEndorsedAsMaster(foreignEndorsableNftId, address(this), nftId), "foreignEndorsableNft is not CrossEndorsedAsMaster");

        _crossEndorsementBy[nftId] = CrossEndorsmentStruct(foreignEndorsableAddress, foreignEndorsableNftId);
    }

    function getTimeLimitedEndorsementInfo(uint nftId, address sealAddress, uint sealNftId) public override view returns (uint endTime) {
        return _timeLimitedEndorsements[nftId][sealAddress][sealNftId];
    }

    function getCrossEndorsementInfo(uint nftId) public override view returns (address endorsableAddress, uint endorsableNftId) {
        return (_crossEndorsementBy[nftId].endorsableAddress, _crossEndorsementBy[nftId].endorsableNftId);
    }

    function isCrossEndorsed(uint nftId) public override view returns (bool) {
        return (_crossEndorsementBy[nftId].endorsableAddress != address(0));
    }

    // function setDraftForever(address draftForever) public override isNotInForever {
    //     require(_isApprovedOrOwner(), "isApprovedOrOwner required");
    //     require(draftForever != address(0), "Draft forever is zero address");
    //     _draftForever = draftForever;
    // }

    // function resetDraftForever() public override onlyDraftForever {
    //     require(_draftForever != address(0), "Draft forever is already zero address");
    //     _draftForever = address(0);
    // }

    // function getForever() public override view returns (address forever) {
    //     return _forever;
    // }

    // function commitForever(uint nftId) public override onlyDraftForever {
    //     require(_seals[nftId].sealAddress != address(0), "Seal is zero address");

    //     _forever = _draftForever;
    //     _draftForever = address(0);
    // }

    function getEndorsementInfo(uint nftId) public view returns (address sealAddress, uint sealNftId, uint8 sealCorner) {
        return (_seals[nftId].sealAddress, _seals[nftId].sealNftId, _seals[nftId].sealCorner);
    }

    function isEndorsed(uint nftId) public override view returns (bool) {
        return _seals[nftId].sealAddress != address(0);
    }

    function supportsInterface(
        bytes4 interfaceId
    ) public view virtual override returns (bool) {
        return interfaceId == type(IERC721Endorsable).interfaceId || super.supportsInterface(interfaceId);
    }

    function _beforeTokenTransfer(address /*from*/, address /*to*/, uint256 tokenId) internal view override isNotInForever {
        require(draftArray.length == 0, "nft is on draft");
    }

    function _baseURI() internal view override returns (string memory) {
        return baseURI_;
    }
}