// SPDX-License-Identifier: BUSL-1.1
pragma solidity ^0.8.9;

interface IERC721Endorsable {
    function getEndorsementInfo(uint nftId) external view returns (address sealAddress, uint sealId, uint8 sealCorner);
    function isEndorsed(uint nftId) external view returns (bool);
    function endorse(uint nftId, address sealAddress, uint sealNftId) external;

    function getTimeLimitedEndorsementInfo(uint nftId, address sealAddress, uint sealNftId) external view returns (uint expirationTime);
    function endorseTimeLimited(uint nftId, address sealAddress, uint sealNftId) external;

    // function setInDraft(uint nftId, bool status) external;

    function crossEndorse(uint nftId, address foreignEndorsableAddress, uint foreignEndorsableNftId) external;
    function getCrossEndorsementInfo(uint nftAddress) external view returns (address endorsableAddress, uint endorsableNftId);
    function isCrossEndorsed(uint nftId) external view returns (bool);
    function markAsCrossEndorsed(uint nftId, address foreignEndorsable, uint endorsableNftId) external;

    function isCrossEndorsedAsMaster(uint nftId, address foreignEndorsableAddress, uint foreignEndorsableNftId) external view returns (bool);

    // function isCrossEndorsedBy(address endorsable) external view returns (bool);
    // function hasCrossEndorsedForeign(address endorsable) external view returns (bool);
    // function endorseCross(uint nftId, address endorsable, uint endorsableNftId) external;

    // function setDraftForever(address draftForever) external;
    // function resetDraftForever() external;

    // function getForever() external view returns (address forever);
    // function commitForever(uint nftId) external;
}
