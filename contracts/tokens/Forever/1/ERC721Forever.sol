// SPDX-License-Identifier: BUSL-1.1
pragma solidity ^0.8.9;

// import "../../extensions/ERC721Single.sol";
// import "./IERC721Forever.sol";
// import "../Endorsable/IERC721Endorsable.sol";
// import "../../interfaces/IERC721.sol";

// contract ERC721Forever is ERC721Single, IERC721Forever {
//     EndorsableTokenInfo[] private _endorsableTokens;

//     constructor(
//         string memory hash_,
//         string memory name_,
//         string memory symbol_,
//         string memory tokenURI_
//     ) ERC721Single(hash_, name_, symbol_, tokenURI_) { }

//     function supportsInterface(
//         bytes4 interfaceId
//     ) public view virtual override(IERC165, ERC721Single) returns (bool) {
//         return interfaceId == type(IERC721Forever).interfaceId || super.supportsInterface(interfaceId);
//     }

//     function getEndorsableTokens() public override view returns (address[] memory endorsableTokens) {
//         for (uint i = 0; i < _endorsableTokens.length; i++) {
//             endorsableTokens[i] = _endorsableTokens[i].endorsable;
//         }
//     }

//     function addEndorsableToken(address endorsable) public override approvedOrOwned {
//         if (_endorsableTokens.length < 4) {
//             address owner = IERC721(endorsable).ownerOf(0);
//             (address seal, uint8 sealCorner) = IERC721Endorsable(endorsable).getEndorsementInfo();

//             _endorsableTokens.push(
//                 EndorsableTokenInfo({
//                     endorsable: endorsable,
//                     owner: owner,
//                     seal: seal,
//                     sealCorner: sealCorner
//                 })
//             );

//             if (_endorsableTokens.length == 4) {
//                 uint8 pos = _endorsableTokens[0].sealCorner;
//                 bool bError = false;

//                 for (uint i = 1; i < 4; i++) {
//                     pos = pos | _endorsableTokens[i].sealCorner;

//                     if (
//                         _endorsableTokens[i].owner != _endorsableTokens[0].owner ||
//                         _endorsableTokens[i].seal  != _endorsableTokens[0].seal
//                     ) {
//                         bError = true;
//                         break;
//                     }
//                 }

//                 if (bError || pos != 15) {
//                     IERC721Endorsable(_endorsableTokens[0].endorsable).resetDraftForever();
//                     IERC721Endorsable(_endorsableTokens[1].endorsable).resetDraftForever();
//                     IERC721Endorsable(_endorsableTokens[2].endorsable).resetDraftForever();
//                     IERC721Endorsable(_endorsableTokens[3].endorsable).resetDraftForever();

//                     selfdestruct(payable(_owner));
//                 } else {
//                     IERC721Endorsable(_endorsableTokens[0].endorsable).commitForever();
//                     IERC721Endorsable(_endorsableTokens[1].endorsable).commitForever();
//                     IERC721Endorsable(_endorsableTokens[2].endorsable).commitForever();
//                     IERC721Endorsable(_endorsableTokens[3].endorsable).commitForever();
//                 }
//             }
//         }
//     }
// }