// SPDX-License-Identifier: BUSL-1.1
pragma solidity ^0.8.9;

interface IERC721Forever {
    struct EndorsableTokenInfo {
        address endorsable;
        address owner;
        address seal;
        uint8 sealCorner;
    }

    function getEndorsableTokens() external view returns (address[] memory endorsableTokens);
    function addEndorsableToken(address endorsable) external;
}