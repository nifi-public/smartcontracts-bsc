// SPDX-License-Identifier: BUSL-1.1
pragma solidity ^0.8.9;

import "@openzeppelin/contracts/token/ERC721/extensions/ERC721Burnable.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

import "./IERC721Seal.sol";

contract ERC721Seal is ERC721Burnable, IERC721Seal, Ownable {

    struct EndorsedEntry {
        bool isEndorsed;
        uint8 sealCorner;
    }

    struct EndorsedTimeLimitedEntry {
        uint endTime;
    }

    string private baseURI_;

    uint public lastTokenId;

    // sealNftId => endorsableAddress => endorsableNftId
    mapping(uint => mapping(address => mapping(uint => EndorsedEntry))) private _endorsed;
    mapping(uint => mapping(address => mapping(uint => EndorsedTimeLimitedEntry))) private _endorsedTimeLimited;

    constructor(
        string memory name_,
        string memory symbol_,
        string memory tokenURI_
    ) ERC721(name_, symbol_) { 
        baseURI_ = tokenURI_;
    }

    // !!! mint is available to anybody, not only Collection Owner
    // !!! need Creator in minted token
    // !!! need CreatorPercent in minted token
    function mint() public onlyOwner {  // NOT onlyOwner!!!
        lastTokenId++;
        _mint(msg.sender, lastTokenId);
    }

    function markAsEndorsed(uint sealNftId, address endorsableAddress, uint endorsableNftId, uint8 sealCorner) public override {
        require(_isApprovedOrOwner(_msgSender(), sealNftId), "isApprovedOrOwner required");
        require(endorsableAddress != address(0), "Endorsable token is zero address");
        require(!_endorsed[sealNftId][endorsableAddress][endorsableNftId].isEndorsed, "Endorsable token is already endorsed");

        _endorsed[sealNftId][endorsableAddress][endorsableNftId] = EndorsedEntry(true, sealCorner);
    }

    function markAsEndorsedTimeLimited(uint sealNftId, address endorsableAddress, uint endorsableNftId, uint endTime) public override {
        require(_isApprovedOrOwner(_msgSender(), sealNftId), "isApprovedOrOwner required");
        require(endorsableAddress != address(0), "Endorsable token is zero address");

        require(
            endTime > _endorsedTimeLimited[sealNftId][endorsableAddress][endorsableNftId].endTime,
            "New end time shall exceed current end time"
        );

        // why only endTime as an argument? should there also be True?
        _endorsedTimeLimited[sealNftId][endorsableAddress][endorsableNftId] = EndorsedTimeLimitedEntry(endTime);
    }

    function getEndorsementInfo(
        uint sealNftId, address endorsableAddress, uint endorsableNftId
    ) public override view returns (bool isEndorsed, uint8 sealCorner) {
        EndorsedEntry memory entry = _endorsed[sealNftId][endorsableAddress][endorsableNftId];

        return (
            entry.isEndorsed,
            entry.sealCorner
        );
    }

    function getTimeLimitedEndorsementInfo(
        uint sealNftId, address endorsableAddress, uint endorsableNftId
    ) public override view returns (bool isEndorsed, uint endTime) {
        EndorsedTimeLimitedEntry storage entry = _endorsedTimeLimited[sealNftId][endorsableAddress][endorsableNftId];

        return (
            block.timestamp < entry.endTime,
            entry.endTime
        );
    }

    function supportsInterface(
        bytes4 interfaceId
    ) public view virtual override returns (bool) {
        return interfaceId == type(IERC721Seal).interfaceId || super.supportsInterface(interfaceId);
    }

    // need GetNftInfo()

    function _baseURI() internal view override returns (string memory) {
        return baseURI_;
    }
}
