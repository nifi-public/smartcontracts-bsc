// SPDX-License-Identifier: BUSL-1.1
pragma solidity ^0.8.9;

interface IERC721Seal {
    function getEndorsementInfo(
        uint sealNftId, address endorsableAddress, uint endorsableNftId
    ) external view returns (bool isEndorsed, uint8 sealCorner);

    function getTimeLimitedEndorsementInfo(
        uint sealNftId, address endorsableAddress, uint endorsableNftId
    ) external view returns (bool isEndorsed, uint endTime);

    function markAsEndorsed(uint sealNftId, address endorsableAddress, uint endorsableNftId, uint8 sealCorner) external;
    function markAsEndorsedTimeLimited(uint sealNftId, address endorsableAddress, uint endorsableNftId, uint endTime) external;
}