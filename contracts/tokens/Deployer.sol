// SPDX-License-Identifier: BUSL-1.1
pragma solidity ^0.8.9;

import "./Endorsable/1/ERC721Endorsable.sol";
import "./Forever/1/ERC721Forever.sol";
import "./Seal/1/ERC721Seal.sol";

contract Deployer {
    struct CreateTokenParams {
        string name;
        string symbol;
        string tokenURI;
    }

    event Endorsable(address token, CreateTokenParams params);
    event Forever(address token, CreateTokenParams params);
    event Seal(address token, CreateTokenParams params);

    function createEndorsable(CreateTokenParams memory params) public returns (address) {
        ERC721Endorsable token = new ERC721Endorsable(
            params.name, params.symbol, params.tokenURI
        );

        emit Endorsable(address(token), params);
        return address(token);
    }

    // function createForever(CreateTokenParams memory params) public returns (address) {
    //     ERC721Forever token = new ERC721Forever(
    //         params.hash, params.name, params.symbol, params.tokenURI
    //     );

    //     emit Forever(address(token), params);
    //     return address(token);
    // }

    function createSeal(CreateTokenParams memory params) public returns (address) {
        ERC721Seal token = new ERC721Seal(
            params.name, params.symbol, params.tokenURI
        );

        emit Seal(address(token), params);
        return address(token);
    }
}