// SPDX-License-Identifier: BUSL-1.1
pragma solidity ^0.8.9;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC721/IERC721.sol";
import "@openzeppelin/contracts/utils/introspection/ERC165Checker.sol";

import "../../tokens/Endorsable/1/IERC721Endorsable.sol";
import "../../tokens/Seal/1/IERC721Seal.sol";

contract EndorsementCross is Ownable {
    struct EndorseRequest {
        bool isActive;
        uint256 price;
    }

    // endorsableAddress, endorsableId, crossEndorsableAddress, crossEndorsableNftId
    mapping(address => mapping(uint => mapping(address => mapping(uint => EndorseRequest)))) public requests;
    mapping(address => mapping(uint => mapping(address => mapping(uint => bool)))) public endorsed;
    
    address payable feeReceiver;
    // stable in WEI
    uint public endorsmentRequestFee;
    // 1e18 - 100%
    uint public endorsmentFee;

    event NewEndorsementRequest(
        address endorsableAddress,
        uint endorsableNftId,
        address crossEndorsableAddress,
        uint crossEndorsableNftId,
        uint price
    );

    event EndorsementRequestCancelled(
        address endorsableAddress,
        uint endorsableNftId,
        address crossEndorsableAddress,
        uint crossEndorsableNftId
    );

    event EndorsementRequestSucceed(
        address endorsableAddress,
        uint endorsableNftId,
        address crossEndorsableAddress,
        uint crossEndorsableNftId
    );

    modifier assertTokenOwner(address nftAddress, uint nftId) {
        require(IERC721(nftAddress).ownerOf(nftId) == _msgSender(), "You are not the owner of the token");
        _;
    }

    constructor(address payable _feeReceiver, uint _endorsmentRequestFee, uint _endorsmentFee) {
        require(_endorsmentFee < 1e18, "invalid fee");
        feeReceiver = _feeReceiver;
        endorsmentRequestFee = _endorsmentRequestFee;
        endorsmentFee = _endorsmentFee;
    }

    function requestEndorse(
        address endorsableAddress,
        uint endorsableNftId,
        address crossEndorsableAddress,
        uint crossEndorsableNftId,
        uint price
    ) public payable assertTokenOwner(endorsableAddress, endorsableNftId) {
        require(msg.value == price + endorsmentRequestFee, "Invalid value");
        feeReceiver.transfer(endorsmentRequestFee);

        require(
            (!ERC165Checker.supportsInterface(endorsableAddress, type(IERC721Seal).interfaceId) &&
             !ERC165Checker.supportsInterface(crossEndorsableAddress, type(IERC721Seal).interfaceId)
            ),
            "Seal is not accessable in cross endorsment"
        );

        require(!endorsed[endorsableAddress][endorsableNftId][crossEndorsableAddress][crossEndorsableNftId], "nft already endorsed");
        require(ERC165Checker.supportsInterface(endorsableAddress, type(IERC721Endorsable).interfaceId));

        (address _crossEndorsableAddress,) = IERC721Endorsable(endorsableAddress).getCrossEndorsementInfo(endorsableNftId);
        require(_crossEndorsableAddress == address(0), "The given endorsable is already cross endorsed");

        IERC721Endorsable(endorsableAddress).setInDraft(endorsableNftId, true);

        requests[endorsableAddress][endorsableNftId][crossEndorsableAddress][crossEndorsableNftId] = EndorseRequest({
            isActive: true,
            price: price
        });

        emit NewEndorsementRequest(endorsableAddress, endorsableNftId, crossEndorsableAddress, crossEndorsableNftId, price);
    }

    function cancelEndorse(
        address endorsableAddress,
        uint endorsableNftId,
        address crossEndorsableAddress,
        uint crossEndorsableNftId
    ) public assertTokenOwner(endorsableAddress, endorsableNftId) {
        EndorseRequest storage request = requests[endorsableAddress][endorsableNftId][crossEndorsableAddress][crossEndorsableNftId];

        require(
            request.isActive,
            "You have not requested an endorsement for this token"
        );

        if(ERC165Checker.supportsInterface(endorsableAddress, type(IERC721Endorsable).interfaceId)) {
            IERC721Endorsable(endorsableAddress).setInDraft(endorsableNftId, false);
        }

        request.isActive = false;

        emit EndorsementRequestCancelled(endorsableAddress, endorsableNftId, crossEndorsableAddress, crossEndorsableNftId);
    }

    // function markAsCrossEndorsed(uint nftId, address foreignEndorsableAddress, uint foreignEndorsableNftId)
    
    function endorse(
        address endorsableAddress,
        uint endorsableNftId,
        address crossEndorsableAddress,
        uint crossEndorsableNftId
    ) public assertTokenOwner(crossEndorsableAddress, crossEndorsableNftId) {
        EndorseRequest storage request = requests[endorsableAddress][endorsableNftId][crossEndorsableAddress][crossEndorsableNftId];

        require(request.isActive, "There is no an endorsement for this token");

        require(!endorsed[endorsableAddress][endorsableNftId][crossEndorsableAddress][crossEndorsableNftId], "nft already endorsed");
        endorsed[endorsableAddress][endorsableNftId][crossEndorsableAddress][crossEndorsableNftId] = true;

        IERC721Endorsable(crossEndorsableAddress).crossEndorse(crossEndorsableNftId, endorsableAddress, endorsableNftId);

        IERC721Endorsable(endorsableAddress).setInDraft(endorsableNftId, false);
        IERC721Endorsable(endorsableAddress).markAsCrossEndorsed(endorsableNftId, crossEndorsableAddress, crossEndorsableNftId);

        uint fee = request.price * endorsmentFee / 1e18;
        feeReceiver.transfer(fee);
        payable(IERC721(crossEndorsableAddress).ownerOf(crossEndorsableNftId)).transfer(request.price - fee);

        request.isActive = false;

        emit EndorsementRequestSucceed(endorsableAddress, endorsableNftId, crossEndorsableAddress, crossEndorsableNftId);
    }
}