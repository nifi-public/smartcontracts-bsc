// SPDX-License-Identifier: BUSL-1.1
pragma solidity ^0.8.9;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC721/IERC721.sol";
import "@openzeppelin/contracts/utils/introspection/ERC165Checker.sol";

import "../../tokens/Endorsable/1/IERC721Endorsable.sol";
import "../../tokens/Seal/1/IERC721Seal.sol";

contract EndorsementTimeLimited is Ownable {
    struct EndorseRequest {
        bool isActive;
        uint256 price;
        uint endTime;
    }

    // endorsableAddress => endorsableId => sealAddress => sealNftId => EndorseRequest
    mapping(address => mapping(uint => mapping(address => mapping(uint => EndorseRequest)))) public requests;
    // endorsableAddress => endorsableId => sealAddress => sealNftId => timestamp
    mapping(address => mapping(uint => mapping(address => mapping(uint => uint)))) public endorsed;
    
    address payable feeReceiver;
    // stable in WEI
    uint public endorsmentRequestFee;
    // 1e18 - 100%
    uint public endorsmentFee;

    event NewEndorsementRequest(
        address endorsableAddress,
        uint endorsableNftId,
        address sealAddress,
        uint sealNftId,
        uint endTime,
        uint price
    );

    event EndorsementRequestCancelled(
        address endorsableAddress,
        uint endorsableNftId,
        address sealAddress,
        uint sealNftId
    );

    event EndorsementRequestSucceed(
        address endorsableAddress,
        uint endorsableNftId,
        address sealAddress,
        uint sealNftId
    );

    modifier assertTokenOwner(address nftAddress, uint nftId) {
        require(IERC721(nftAddress).ownerOf(nftId) == _msgSender(), "You are not the owner of the token");
        _;
    }

    constructor(address payable _feeReceiver, uint _endorsmentRequestFee, uint _endorsmentFee) {
        require(_endorsmentFee < 1e18, "invalid fee");
        feeReceiver = _feeReceiver;
        endorsmentRequestFee = _endorsmentRequestFee;
        endorsmentFee = _endorsmentFee;
    }

    function requestEndorse(
        address endorsableAddress,
        uint endorsableNftId,
        address sealAddress,
        uint sealNftId,
        uint endTime,
        uint price
    ) public payable assertTokenOwner(endorsableAddress, endorsableNftId) {
        require(msg.value == price + endorsmentRequestFee, "Invalid value");
        feeReceiver.transfer(endorsmentRequestFee);

        require(
            !requests[endorsableAddress][endorsableNftId][sealAddress][sealNftId].isActive,
            "You have already requested an endorsement for this token"
        );

        require(
            ERC165Checker.supportsInterface(sealAddress, type(IERC721Seal).interfaceId),
            "The given seal does not support the IERC721Seal interface"
        );

        require(endTime > block.timestamp + 1 days, "end time cannot be less than now + 24 hours");

        require(endTime > endorsed[endorsableAddress][endorsableNftId][sealAddress][sealNftId], "nft already endorsed on requested endTime");
        if(ERC165Checker.supportsInterface(endorsableAddress, type(IERC721Endorsable).interfaceId)) {
            IERC721Endorsable(endorsableAddress).setInDraft(endorsableNftId, true);
        }

        requests[endorsableAddress][endorsableNftId][sealAddress][sealNftId] = EndorseRequest({
            isActive: true,
            price: price,
            endTime: endTime
        });

        emit NewEndorsementRequest(endorsableAddress, endorsableNftId, sealAddress, sealNftId, endTime, price);
    }

    function cancelEndorse(
        address endorsableAddress,
        uint endorsableNftId,
        address sealAddress,
        uint sealNftId
    ) public assertTokenOwner(endorsableAddress, endorsableNftId) {
        EndorseRequest storage request = requests[endorsableAddress][endorsableNftId][sealAddress][sealNftId];

        require(
            request.isActive,
            "You have not requested an endorsement for this token"
        );

        if(ERC165Checker.supportsInterface(endorsableAddress, type(IERC721Endorsable).interfaceId)) {
            IERC721Endorsable(endorsableAddress).setInDraft(endorsableNftId, false);
        }

        request.isActive = false;

        emit EndorsementRequestCancelled(endorsableAddress, endorsableNftId, sealAddress, sealNftId);
    }

    function endorse(
        address endorsableAddress,
        uint endorsableNftId,
        address sealAddress,
        uint sealNftId
    ) public assertTokenOwner(sealAddress, sealNftId) {
        EndorseRequest storage request = requests[endorsableAddress][endorsableNftId][sealAddress][sealNftId];

        require(request.isActive, "There is no an endorsement for this token");

        IERC721Seal(sealAddress).markAsEndorsedTimeLimited(
            sealNftId,
            endorsableAddress,
            endorsableNftId,
            request.endTime
        );

        require(request.endTime > endorsed[endorsableAddress][endorsableNftId][sealAddress][sealNftId], "nft already endorsed");
        endorsed[endorsableAddress][endorsableNftId][sealAddress][sealNftId] = request.endTime;

        if(ERC165Checker.supportsInterface(endorsableAddress, type(IERC721Endorsable).interfaceId)) {
            IERC721Endorsable(endorsableAddress).setInDraft(endorsableNftId, false);
            IERC721Endorsable(endorsableAddress).endorseTimeLimited(endorsableNftId, sealAddress, sealNftId);
        }

        uint fee = request.price * endorsmentFee / 1e18;
        feeReceiver.transfer(fee);
        payable(IERC721(sealAddress).ownerOf(sealNftId)).transfer(request.price - fee);

        request.isActive = false;

        emit EndorsementRequestSucceed(endorsableAddress, endorsableNftId, sealAddress, sealNftId);
    }
}