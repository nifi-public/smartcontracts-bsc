// SPDX-License-Identifier: BUSL-1.1
pragma solidity ^0.8.9;

import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "@openzeppelin/contracts/token/ERC721/IERC721.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/introspection/ERC165Checker.sol";

contract Ask is Ownable {

    using SafeERC20 for IERC20;

    struct AskInfo {
        bool isActive;
        address tokenAddress;
        uint price;
        address payable owner;
        uint expirationTime;
    }

    mapping(address => mapping(uint => AskInfo)) public _asks;

    modifier assertTokenOwner(address tokenAddress, uint nftId) {
        require(IERC721(tokenAddress).ownerOf(nftId) == _msgSender(), "You are not the owner of the token");
        _;
    }

    modifier activeAsk(address tokenAddress, uint nftId) {
        require(_asks[tokenAddress][nftId].isActive, "There is no active ask for this token");
        require(_asks[tokenAddress][nftId].expirationTime > block.timestamp, "The ask has expired");
        _;
    }

    function createAsk(address nftAddress, uint nftId, address tokenAddress, uint256 price, uint32 expirationTime) public assertTokenOwner(nftAddress, nftId) {
        require(price > 0, "Invalid price");
        require(!_asks[nftAddress][nftId].isActive, "You have already created an ask for this token");

        _asks[nftAddress][nftId] = AskInfo({
            isActive: true,
            tokenAddress: tokenAddress,
            price: price,
            owner: payable(_msgSender()),
            expirationTime: expirationTime
        });
    }

    function changePrice(address nftAddress, uint nftId, uint256 price) public assertTokenOwner(nftAddress, nftId) activeAsk(nftAddress, nftId) {
        require(price > 0, "Invalid price");
        _asks[nftAddress][nftId].price = price;
    }

    function cancelAsk(address nftAddress, uint nftId) public assertTokenOwner(nftAddress, nftId) activeAsk(nftAddress, nftId) {
        _asks[nftAddress][nftId].isActive = false;
    }

    function purchase(address nftAddress, uint nftId) public payable activeAsk(nftAddress, nftId) {
        AskInfo storage ask = _asks[nftAddress][nftId];

        require(ask.isActive, "There is no active ask for this token");
        require(ask.owner != _msgSender(), "You are the owner of the token");

        IERC721(nftAddress).safeTransferFrom(ask.owner, _msgSender(), nftId);

        if(ask.tokenAddress == address(0)) {
            require(ask.price == msg.value, "Invalid value");
            ask.owner.transfer(ask.price);
        } else {
            IERC20(ask.tokenAddress).safeTransferFrom(msg.sender, ask.owner, ask.price);
        }
        
        ask.isActive = false;
    }
}